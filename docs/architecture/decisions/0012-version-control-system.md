# 12. version-control-system

Date: 2023-04-10

## Status

Accepted

## Context

TTA 실습에 필요한 기본 환경 배포를 무료로 하려고 ㅎ나다.
github이 더 많은 사용자를 가지고 있지만, gitlab은 오픈소스여서
나중에 on-premise로 이전하기 쉬울 것 같다.

~ The issue motivating this decision, and any context that influences or constrains the decision.

## Decision

버전 관리 시스템을 gitlab을 쓰겠다.
~ The change that we're proposing or have agreed to implement.

## Consequences

gitlab 계정을 만들고, 필요한 파일을 등록한다.
~ What becomes easier or more difficult to do and any risks introduced by the change that will need to be mitigated.
