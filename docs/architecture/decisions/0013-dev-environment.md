# 13. dev-environment

Date: 2023-04-10

## Status

Accepted

## Context

TTA 실습에 각 PC마다 환경 설정이 어렵다.
그래서 실제 실습 시간이 부족한다.

## Decision

실습용으로 무료 서비스인 gitpod을 한다.

## Consequences

gitpod에 실습용 환경을 docker로 구성하여 자동으로 동작할 수 있게 한다.
